run:
	go run ./cmd

tidy:
	go mod tidy

build:
	go build -o ./build/main.exe ./cmd/main.go

protoc:
	protoc \
	-I protos protos/proto/*.proto \
	--go_out=./protos/gen/go/ \
	--go_opt=paths=import \
	--go-grpc_out=./protos/gen/go/ \
	--go-grpc_opt=paths=import

compose:
	docker compose up -d