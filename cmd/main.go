package main

import (
	"log"

	"project-template/internal/app"
	"project-template/internal/config"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("failed init config: %s", err)
	}
	application, err := app.New(cfg)
	if err != nil {
		log.Fatalf("failed init application: %s", err)
	}
	application.Run()
}
