package scylla

import (
	"strconv"

	"project-template/internal/config"
	"project-template/pkg/logger"

	"github.com/gocql/gocql"
)

type Scylla struct {
	Session *gocql.Session
}

func New(cfg *config.Scylla, log *logger.Logger) (*Scylla, error) {
	var err error

	c := gocql.NewCluster(cfg.Host)
	c.Port, err = strconv.Atoi(cfg.Port)
	if err != nil {
		return nil, err
	}
	c.Logger = log
	c.Keyspace = cfg.Keyspace
	c.Authenticator = gocql.PasswordAuthenticator{
		Username: cfg.User,
		Password: cfg.Pass,
	}

	s, err := c.CreateSession()
	if err != nil {
		return nil, err
	}

	return &Scylla{
		Session: s,
	}, nil
}

func (s *Scylla) Stop() {
	s.Session.Close()
}
