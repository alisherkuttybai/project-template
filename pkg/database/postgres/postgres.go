package postgres

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/file"
	"time"

	"project-template/internal/config"

	_ "github.com/jackc/pgx/v5"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
)

type Postgres struct {
	Conn *sqlx.DB
	url  string
}

func New(cfg *config.Postgres) (*Postgres, error) {
	url := postgresURL(cfg)
	db, err := sqlx.Open("pgx", url)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	return &Postgres{
		Conn: db,
		url:  url,
	}, nil
}

func (db *Postgres) Migrate(cfg *config.Postgres) error {
	instance, err := postgres.WithInstance(db.Conn.DB, &postgres.Config{})
	if err != nil {
		return err
	}

	files, err := (&file.File{}).Open("./migrations")
	if err != nil {
		return err
	}

	m, err := migrate.NewWithInstance("file", files, cfg.Name, instance)
	if err != nil {
		return err
	}

	if err = m.Up(); err != nil && !errors.Is(err, errors.New("no change")) {
		return err
	}

	return nil
}

func (db *Postgres) Ping(ctx context.Context) error {
	if db == nil || db.Conn == nil {
		return db.Reconnect(ctx)
	}
	return nil
}

func (db *Postgres) Reconnect(ctx context.Context) error {
	var err error
	db.Conn, err = sqlx.Open("postgres", db.url)
	if err != nil {
		return err
	}
	if err := db.Conn.PingContext(ctx); err != nil {
		return err
	}
	return nil
}

func (db *Postgres) Stop() error {
	return db.Conn.Close()
}

func postgresURL(cfg *config.Postgres) string {
	return fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
		cfg.User, cfg.Pass, cfg.Host, cfg.Port, cfg.Name)
}
