package oracle

import (
	"context"
	"database/sql"
	"fmt"
	"project-template/internal/config"
	"time"

	"github.com/godror/godror"
)

type Oracle struct {
	Conn *sql.DB
	url  string
}

func NewOracleSQLReportRepository(cfg *config.Oracle) (*Oracle, error) {
	url := oracleURL(cfg)

	var params godror.ConnectionParams
	params.Username = cfg.User
	params.Password = godror.NewPassword(cfg.Pass)
	params.ConnectString = url
	params.SessionTimeout = 42 * time.Second
	params.ConfigDir = "/tmp/admin"
	params.Heterogeneous = false
	params.StandaloneConnection = false

	conn, err := sql.Open("godror", params.StringWithPassword())
	if err != nil {
		return nil, err
	}

	ctx, cncl := context.WithTimeout(context.Background(), 30*time.Second)
	defer cncl()
	err = conn.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	return &Oracle{
		Conn: conn,
		url:  url,
	}, nil
}

func (db *Oracle) Ping() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	return db.Conn.PingContext(ctx)
}

func oracleURL(cfg *config.Oracle) string {
	return fmt.Sprintf("%s:%s/%s", cfg.Host, cfg.Port, cfg.Name)
}
