package grpcserver

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"project-template/internal/config"
)

type GRPCServer struct {
	Server  *grpc.Server
	address string
}

func New(cfg *config.GRPC) *GRPCServer {
	return &GRPCServer{
		Server:  grpc.NewServer(),
		address: fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
	}
}

func (srv *GRPCServer) Start() error {
	l, err := net.Listen("tcp", srv.address)
	if err != nil {
		return err
	}
	if err := srv.Server.Serve(l); err != nil {
		return err
	}
	return nil
}

func (srv *GRPCServer) Stop() {
	srv.Server.GracefulStop()
}
