package httpserver

import (
	"fmt"
	"github.com/rs/cors"
	"net/http"

	"github.com/gin-gonic/gin"
	"project-template/internal/config"
)

const (
	logProd = "prod"
)

type Server struct {
	HttpServer *http.Server
	Router     *gin.Engine
}

func New(logLvl string, cfg *config.HTTP) (*Server, error) {
	if logLvl == logProd {
		gin.SetMode(gin.ReleaseMode)
	}

	srv := &Server{
		Router: gin.New(),
	}
	srv.HttpServer = &http.Server{
		Addr:    fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		Handler: cors.AllowAll().Handler(srv),
	}
	return srv, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Router.ServeHTTP(w, r)
}
