package logger

import (
	"errors"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"project-template/pkg/logger/slogpretty"
)

const (
	logLocal = "local"
	logDev   = "dev"
	logProd  = "prod"
)

var errNotValidType = errors.New("not valid logger type")

type Logger struct {
	*slog.Logger
}

func New(logLvl string) (*Logger, error) {
	var (
		logger   *slog.Logger
		logLevel = strings.ToLower(logLvl)
	)
	switch logLevel {
	case logLocal:
		logger = setupPrettySlog()
	case logDev:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
				AddSource: true,
				Level:     slog.LevelDebug,
			}),
		)
	case logProd:
		logger = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
				AddSource: true,
				Level:     slog.LevelInfo,
			}),
		)
	default:
		return nil, errNotValidType
	}

	return &Logger{Logger: logger}, nil
}

// implementation method for interface logger of gocql library
func (l *Logger) Print(v ...interface{}) {
	l.Error("lib error", slog.Any("error", v))
}

// implementation method for interface logger of gocql library
func (l *Logger) Printf(format string, v ...interface{}) {
	l.Error("lib error", slog.Any("error", fmt.Errorf(format, v...)))
}

// implementation method for interface logger of gocql library
func (l *Logger) Println(v ...interface{}) {
	l.Error("lib error", slog.Any("error", v))
}

func setupPrettySlog() *slog.Logger {
	options := slogpretty.PrettyHandlerOptions{
		SlogOpts: &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
		},
	}
	handler := options.NewPrettyHandler(os.Stdout)
	return slog.New(handler)
}

func Err(err error) slog.Attr {
	return slog.Attr{
		Key:   "error",
		Value: slog.StringValue(err.Error()),
	}
}
