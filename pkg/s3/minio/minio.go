package minio

import (
	"bytes"
	"context"
	"github.com/minio/minio-go/v7"
	"net/url"
	"time"

	"github.com/minio/minio-go/v7/pkg/credentials"
	"project-template/internal/config"
)

type Minio struct {
	client *minio.Client
	params *config.Minio
}

func New(cfg *config.Minio) (*Minio, error) {
	client, err := minio.New(cfg.Url, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.AccessKey, cfg.SecretKey, ""),
		Secure: false,
	})
	if err != nil {
		return nil, err
	}

	return &Minio{
		client: client,
		params: cfg,
	}, nil
}

func (m *Minio) UploadFile(ctx context.Context, objectName string, reader *bytes.Reader) error {
	if _, err := m.client.PutObject(
		ctx,
		m.params.BucketName,
		objectName,
		reader,
		-1,
		minio.PutObjectOptions{}); err != nil {
		return err
	}
	return nil
}

func (m *Minio) GetFileLink(ctx context.Context, objectName string, reader *bytes.Reader) (*url.URL, error) {
	expiry := 604800 * time.Second
	fileUrl, err := m.client.PresignedGetObject(ctx, m.params.BucketName, objectName, expiry, nil)
	if err != nil {
		return nil, err
	}
	return fileUrl, nil
}
