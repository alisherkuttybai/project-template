package kafka

import (
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"strings"

	"project-template/internal/config"
)

type Kafka struct {
	Writer *kafka.Writer
	Reader *kafka.Reader
}

func New(cfg *config.Kafka) (*Kafka, error) {
	brokers := strings.Join(cfg.Brokers, ",")
	w := kafka.Writer{
		Addr:         kafka.TCP(brokers),
		Topic:        cfg.Topic,
		Balancer:     &kafka.Hash{},
		WriteTimeout: cfg.Timeout,
	}

	r := kafka.ReaderConfig{
		Brokers:  cfg.Brokers,
		Topic:    cfg.Topic,
		MaxBytes: 10e6,
		GroupID:  cfg.GroupID,
	}

	return &Kafka{
		Writer: &w,
		Reader: kafka.NewReader(r),
	}, nil
}

func (mq *Kafka) Write(ctx context.Context, message []byte) error {
	return mq.Writer.WriteMessages(ctx, kafka.Message{Value: message})
}

func (mq *Kafka) Read(ctx context.Context) (interface{}, error) {
	var msg interface{}
	m, err := mq.Reader.ReadMessage(ctx)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		return nil, err
	}

	return msg, nil
}

func (mq *Kafka) Stop() error {
	return mq.Reader.Close()
}
