package sample

import (
	"github.com/jmoiron/sqlx"
	modelsample "project-template/internal/model/sample"
	"project-template/pkg/logger"
)

type Sample struct {
	log  *logger.Logger
	conn *sqlx.DB
}

func New(log *logger.Logger, conn *sqlx.DB) *Sample {
	return &Sample{
		log:  log,
		conn: conn,
	}
}

func (s *Sample) GetConn() *sqlx.DB {
	return s.conn
}

func (s *Sample) GetObjects() ([]*modelsample.Sample, error) {
	return nil, nil
}
