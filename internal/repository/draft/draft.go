package draft

import (
	"context"
	"github.com/gocql/gocql"
	modeldraft "project-template/internal/model/draft"
	"project-template/pkg/logger"
)

type Draft struct {
	log  *logger.Logger
	conn *gocql.Session
}

func New(log *logger.Logger, conn *gocql.Session) *Draft {
	return &Draft{
		log:  log,
		conn: conn,
	}
}

func (d *Draft) GetConn() *gocql.Session {
	return d.conn
}

func (d *Draft) SaveDraft(ctx context.Context, draft *modeldraft.Draft) error {
	return nil
}
