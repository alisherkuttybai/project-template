package repository

import (
	"github.com/gocql/gocql"
	"github.com/jmoiron/sqlx"
	"project-template/internal/repository/draft"
	"project-template/internal/repository/sample"
	"project-template/pkg/logger"
)

type Repository struct {
	Sample *sample.Sample
	Draft  *draft.Draft
}

func New(log *logger.Logger, conn *sqlx.DB, session *gocql.Session) *Repository {
	return &Repository{
		Sample: sample.New(log, conn),
		Draft:  draft.New(log, session),
	}
}
