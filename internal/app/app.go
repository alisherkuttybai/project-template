package app

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"project-template/internal/config"
	"project-template/pkg/database/scylla"
	"project-template/pkg/logger"
	"project-template/pkg/mq/kafka"
	"project-template/pkg/s3/minio"
	"project-template/pkg/transport/httpserver"
	"syscall"
)

type App struct {
	log *logger.Logger

	db  *scylla.Scylla
	s3  *minio.Minio
	mq  *kafka.Kafka
	srv *httpserver.Server

	//repo *repository.Repository
	//u *usecase.Usecase
	//c *controller.Controller
}

func New(cfg *config.Config) (*App, error) {
	log, err := logger.New(cfg.App.LogLevel)
	if err != nil {
		return nil, fmt.Errorf("failed init logger: %w", err)
	}

	return &App{
		log: log,
	}, nil
}

func (a *App) Run() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	a.log.Info("application starting")
	a.log.Info("application started")
	ctx := context.Background()

	sign := <-stop
	a.log.Info("stopping application", slog.String("signal", sign.String()))

	a.Stop(ctx)
	a.log.Info("application stopped")
}

func (a *App) Stop(ctx context.Context) {

}
