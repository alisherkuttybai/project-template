package draft

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	modeldraft "project-template/internal/model/draft"
	"project-template/pkg/logger"
)

type DraftUsecase interface {
	SaveDraft(ctx context.Context, draft *modeldraft.Draft) error
}

type Draft struct {
	log      *logger.Logger
	usecase  DraftUsecase
	validate *validator.Validate
}

func New(log *logger.Logger, u DraftUsecase, v *validator.Validate) *Draft {
	return &Draft{
		log:      log,
		usecase:  u,
		validate: v,
	}
}

func (d *Draft) SaveDraft(c *gin.Context) {
}
