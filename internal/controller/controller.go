package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"project-template/internal/config"
	"project-template/internal/controller/draft"
	"project-template/internal/controller/sample"
	"project-template/internal/usecase"
	"project-template/pkg/api"
	"project-template/pkg/logger"
	"project-template/pkg/transport/httpserver/middleware"
)

type Controller struct {
	cfg    *config.HTTP
	log    *logger.Logger
	router *gin.Engine
	sample *sample.Sample
	draft  *draft.Draft
}

func New(
	cfg *config.HTTP,
	log *logger.Logger,
	r *gin.Engine,
	usecase *usecase.Usecase,
	validate *validator.Validate,
) *Controller {
	c := &Controller{
		cfg:    cfg,
		log:    log,
		router: r,
		sample: sample.New(log, usecase.Sample),
		draft:  draft.New(log, usecase.Draft, validate),
	}
	c.setupRoutes()
	return c
}

func (c *Controller) setupRoutes() {
	srv := c.router
	srv.Use(middleware.SetRequestID())
	srv.Use(middleware.LogRequest(c.log.Logger))

	srv.NoRoute(func(c *gin.Context) {
		api.ResponseNotFound(c, "not found")
	})
	srv.NoMethod(func(c *gin.Context) {
		api.ResponseMethodNotAllowed(c, "method not allowed")
	})
	srv.GET("/health", func(c *gin.Context) {
		api.ResponseOK(c, "application is alive")
	})

	gRoute := srv.Group(c.cfg.PathPrefix)
	gAudit := gRoute.Group("/templates")
	gAudit.GET("/get-samples", c.sample.GetObjects)
	gAudit.POST("/save-draft", c.draft.SaveDraft)
}
