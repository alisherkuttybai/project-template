package sample

import (
	"github.com/gin-gonic/gin"
	modelsample "project-template/internal/model/sample"
	"project-template/pkg/logger"
)

type SampleUsecase interface {
	GetObjects() ([]*modelsample.Sample, error)
}

type Sample struct {
	log     *logger.Logger
	usecase SampleUsecase
}

func New(log *logger.Logger, u SampleUsecase) *Sample {
	return &Sample{
		log:     log,
		usecase: u,
	}
}

func (s *Sample) GetObjects(c *gin.Context) {
}
