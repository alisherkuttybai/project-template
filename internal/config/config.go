package config

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"os"
	"strings"
	"time"
)

const (
	configPath = "./config/config.yaml"
)

type Config struct {
	App       App       `yaml:"APP" env:"APP"`
	Transport Transport `yaml:"TRANSPORT" env:"TRANSPORT"`
	DB        DB        `yaml:"DB" env:"DB"`
	MQ        MQ        `yaml:"MQ" env:"MQ"`
	S3        S3        `yaml:"S3" env:"S3"`
}

type App struct {
	LogLevel string `yaml:"LOG_LEVEL" env:"APP_LOG_LEVEL" env-required:"true"`
	Name     string `yaml:"NAME" env:"APP_NAME" env-required:"true"`
}

type Transport struct {
	HTTP HTTP `yaml:"HTTP" env:"HTTP"`
	GRPC GRPC `yaml:"GRPC" env:"GRPC"`
}

type HTTP struct {
	Host       string `yaml:"HOST" env:"HTTP_HOST" env-required:"true"`
	Port       string `yaml:"PORT" env:"HTTP_PORT" env-required:"true"`
	PathPrefix string `yaml:"PATH_PREFIX" env:"HTTP_PATH_PREFIX" env-required:"true"`
}

type GRPC struct {
	Host    string        `yaml:"HOST" env:"GRPC_PORT" env-required:"true"`
	Port    string        `yaml:"PORT" env:"GRPC_HOST" env-required:"true"`
	Timeout time.Duration `yaml:"TIMEOUT" env:"GRPC_TIMEOUT" env-required:"true"`
}

type DB struct {
	Postgres Postgres `yaml:"POSTGRES" env:"POSTGRES"`
	Scylla   Scylla   `yaml:"SCYLLA" env:"SCYLLA"`
	Oracle   Oracle   `yaml:"ORACLE" env:"ORACLE"`
}

type Postgres struct {
	Host string `yaml:"HOST" env:"POSTGRES_HOST" env-required:"true"`
	Port string `yaml:"PORT" env:"POSTGRES_PORT" env-required:"true"`
	User string `yaml:"USER" env:"POSTGRES_USER" env-required:"true"`
	Pass string `yaml:"PASS" env:"POSTGRES_PASS" env-required:"true"`
	Name string `yaml:"NAME" env:"POSTGRES_NAME" env-required:"true"`
}

type Scylla struct {
	Host     string `yaml:"HOST" env:"SCYLLA_PORT" env-required:"true"`
	Port     string `yaml:"PORT" env:"SCYLLA_HOST" env-required:"true"`
	User     string `yaml:"USER" env:"SCYLLA_USER" env-required:"true"`
	Pass     string `yaml:"PASS" env:"SCYLLA_PASS" env-required:"true"`
	Keyspace string `yaml:"KEYSPACE" env:"SCYLLA_KEYSPACE" env-required:"true"`
}

type Oracle struct {
	Host string `yaml:"HOST" env:"ORACLE_HOST" env-required:"true"`
	Port string `yaml:"PORT" env:"ORACLE_PORT" env-required:"true"`
	User string `yaml:"USER" env:"ORACLE_USER" env-required:"true"`
	Pass string `yaml:"PASS" env:"ORACLE_PASS" env-required:"true"`
	Name string `yaml:"NAME" env:"ORACLE_NAME" env-required:"true"`
}

type MQ struct {
	Kafka Kafka `yaml:"KAFKA" env:"KAFKA"`
}

type Kafka struct {
	Brokers []string      `yaml:"BROKERS" env:"KAFKA_BROKERS" env-required:"true"`
	GroupID string        `yaml:"GROUP_ID" env:"KAFKA_GROUP_ID" env-required:"true"`
	Topic   string        `yaml:"TOPIC" env:"KAFKA_TOPIC" env-required:"true"`
	Timeout time.Duration `yaml:"TIMEOUT" env:"KAFKA_TIMEOUT" env-required:"true"`
}

type S3 struct {
	Minio Minio `yaml:"MINIO" env:"MINIO"`
}

type Minio struct {
	Url        string `yaml:"URL" env:"MINIO_URL" env-required:"true"`
	BucketName string `yaml:"BUCKET_NAME" env:"MINIO_BUCKET_NAME" env-required:"true"`
	AccessKey  string `yaml:"ACCESS_KEY" env:"MINIO_ACCESS_KEY" env-required:"true"`
	SecretKey  string `yaml:"SECRET_KEY" env:"MINIO_SECRET_KEY" env-required:"true"`
}

func New() (*Config, error) {
	cfg := &Config{}
	cfgPath := os.Getenv("CONFIG_PATH")
	if strings.TrimSpace(cfgPath) == "" {
		cfgPath = configPath
	}
	if _, err := os.Stat(cfgPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("config file does not exist: %s", cfgPath)
	}
	if err := cleanenv.ReadConfig(cfgPath, cfg); err != nil {
		return nil, fmt.Errorf("failed read config: %w", err)
	}
	return cfg, nil
}
