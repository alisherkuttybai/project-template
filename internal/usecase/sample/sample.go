package sample

import (
	"github.com/jmoiron/sqlx"
	modelsample "project-template/internal/model/sample"
	"project-template/internal/usecase"
	"project-template/pkg/logger"
)

type sampleRepo interface {
	GetConn() *sqlx.DB
	GetObjects() ([]*modelsample.Sample, error)
}

type Sample struct {
	log  *logger.Logger
	repo sampleRepo
	mq   usecase.MsgQueue
}

func New(log *logger.Logger, repo sampleRepo, mq usecase.MsgQueue) *Sample {
	return &Sample{
		log:  log,
		repo: repo,
		mq:   mq,
	}
}

func (s *Sample) GetObjects() ([]*modelsample.Sample, error) {
	return nil, nil
}
