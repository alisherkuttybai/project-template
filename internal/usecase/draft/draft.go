package draft

import (
	"context"
	"github.com/gocql/gocql"
	modeldraft "project-template/internal/model/draft"
	"project-template/internal/usecase"
	"project-template/pkg/logger"
)

type draftRepo interface {
	GetConn() *gocql.Session
	SaveDraft(ctx context.Context, draft *modeldraft.Draft) error
}

type Draft struct {
	log  *logger.Logger
	repo draftRepo
	s3   usecase.StorageS3
}

func New(log *logger.Logger, repo draftRepo, s3 usecase.StorageS3) *Draft {
	return &Draft{
		log:  log,
		repo: repo,
		s3:   s3,
	}
}

func (d *Draft) SaveDraft(ctx context.Context, draft *modeldraft.Draft) error {
	return nil
}
