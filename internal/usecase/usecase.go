package usecase

import (
	"bytes"
	"context"
	"net/url"
	"project-template/internal/repository"
	"project-template/internal/usecase/draft"
	"project-template/internal/usecase/sample"
	"project-template/pkg/logger"
)

type StorageS3 interface {
	UploadFile(ctx context.Context, objectName string, reader *bytes.Reader) error
	GetFileLink(ctx context.Context, objectName string, reader *bytes.Reader) (*url.URL, error)
}

type MsgQueue interface {
	Write(ctx context.Context, message []byte) error
	Read(ctx context.Context) (interface{}, error)
}

type Usecase struct {
	Sample *sample.Sample
	Draft  *draft.Draft
}

func New(log *logger.Logger, repo *repository.Repository, mq MsgQueue, s3 StorageS3) *Usecase {
	return &Usecase{
		Sample: sample.New(log, repo.Sample, mq),
		Draft:  draft.New(log, repo.Draft, s3),
	}
}
